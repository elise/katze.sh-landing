use axum::{headers::UserAgent, routing::get, Router, TypedHeader};

macro_rules! text {
    ($text:literal) => {
        const BROWSER_TEXT: &str = $text;
        const OTHER_TEXT: &str = concat!("echo \"", $text, '"');
    };
}

text!("\
|\\------/|
   Meow!
Name: Elise
Age: 20
Gender: F
Location: Berlin
Species: Cat
Langaues: en, de(basic conversations)

Matrix: @elise:katze.sh
Fedi: @elise@katze.sh
Twitter: @EliseZeroTwo
osu!: elise

-- Talks --
Breaking the black-box security coprocessor in the Nintendo Switch, a story of vulnerability after vulnerability (GPN21, 2023): https://media.ccc.de/v/gpn21-16-breaking-the-black-box-security-coprocessor-in-the-nintendo-switch-a-story-of-vulnerability-after-vulnerability
");

const BROWSERS: [&str; 7] = [
    "mozilla", "webkit", "chrome", "firefox", "opera", "edge", " msie ",
];

async fn handler(header: TypedHeader<UserAgent>) -> &'static str {
    let lower = header.as_str().to_lowercase();

    match BROWSERS.iter().any(|&val| lower.contains(val)) {
        true => BROWSER_TEXT,
        false => OTHER_TEXT,
    }
}

#[tokio::main]
async fn main() {
    let app = Router::new()
        .route("/", get(handler))
        .route("/health", get(|| async {}));

    axum::Server::bind(&"0.0.0.0:80".parse().unwrap())
        .serve(app.into_make_service())
        .await
        .unwrap();
}
