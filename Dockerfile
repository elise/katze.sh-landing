FROM rust:latest

EXPOSE 80

WORKDIR /usr/src/katzesh
COPY . .

RUN cargo install --path .
CMD ["katzesh"]
